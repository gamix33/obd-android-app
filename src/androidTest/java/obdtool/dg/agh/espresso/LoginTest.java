package obdtool.dg.agh.espresso;


import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.math.BigInteger;
import java.security.SecureRandom;

import obdtool.dg.agh.app.R;
import obdtool.dg.agh.app.view.activities.LoginActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class LoginTest {

    @Rule
    public ActivityTestRule<LoginActivity> mActivityRule = new ActivityTestRule<>(
            LoginActivity.class);


    private static String trueEmail;
    private static String truePass;
    private static String randomEmail;
    private static String randomPass;

    @BeforeClass
    public static void init() {
        trueEmail = "damian@test.pl";
        truePass = "test12345";

        SecureRandom random = new SecureRandom();
        String randomString = new BigInteger(130, random).toString(32);

        randomEmail = randomString+"@test.com";
        randomPass = randomString;
    }


    @Test
    public void failLoginTest(){
        onView(withId(R.id.email)).perform(typeText(randomEmail));
        onView(withId(R.id.password)).perform(typeText(randomPass));
        onView(withId(R.id.loginBtn)).perform(click());
        onView(withId(R.id.login_activity)).check(matches(isDisplayed()));
    }

    @Test
    public void successLoginTest(){
        onView(withId(R.id.email)).perform(typeText(trueEmail));
        onView(withId(R.id.password)).perform(typeText(truePass));
        onView(withId(R.id.loginBtn)).perform(click());
        onView(withId(R.id.dashboard)).check(matches(isDisplayed()));
    }

    @Test
    public void failRegisterTest(){
        onView(withId(R.id.email)).perform(typeText(trueEmail));
        onView(withId(R.id.password)).perform(typeText(truePass));
        onView(withId(R.id.registerBtn)).perform(click());
        onView(withId(R.id.login_activity)).check(matches(isDisplayed()));
    }

    @Test
    public void successRegisterTest(){
        onView(withId(R.id.email)).perform(typeText(randomEmail));
        onView(withId(R.id.password)).perform(typeText(randomPass));
        onView(withId(R.id.registerBtn)).perform(click());
        onView(withId(R.id.dashboard)).check(matches(isDisplayed()));
    }
}
