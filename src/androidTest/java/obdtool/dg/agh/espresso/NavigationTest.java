package obdtool.dg.agh.espresso;


import android.support.test.espresso.contrib.DrawerActions;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import obdtool.dg.agh.app.R;
import obdtool.dg.agh.app.view.activities.MainActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.DrawerMatchers.isClosed;
import static android.support.test.espresso.contrib.DrawerMatchers.isOpen;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class NavigationTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    private void openDrawer(){
        DrawerActions.openDrawer(R.id.drawer_layout);
        onView(withId(R.id.drawer_layout)).check(matches(isOpen()));
    }

    @Test
    public void navigationTest(){
        openDrawer();

        onView(withText(R.string.fault)).perform(click());
        onView(withId(R.id.drawer_layout)).check(matches(isClosed()));
        onView(withId(R.id.faults)).check(matches(isDisplayed()));

        openDrawer();

        onView(withText(R.string.stats)).perform(click());
        onView(withId(R.id.drawer_layout)).check(matches(isClosed()));
        onView(withId(R.id.stats)).check(matches(isDisplayed()));

        openDrawer();

        onView(withText(R.string.trips)).perform(click());
        onView(withId(R.id.drawer_layout)).check(matches(isClosed()));
        onView(withId(R.id.trips)).check(matches(isDisplayed()));

        openDrawer();

        onView(withText(R.string.log)).perform(click());
        onView(withId(R.id.drawer_layout)).check(matches(isClosed()));
        onView(withId(R.id.log)).check(matches(isDisplayed()));

        openDrawer();

        onView(withText(R.string.dashboard)).perform(click());
        onView(withId(R.id.drawer_layout)).check(matches(isClosed()));
        onView(withId(R.id.dashboard)).check(matches(isDisplayed()));

    }
}
