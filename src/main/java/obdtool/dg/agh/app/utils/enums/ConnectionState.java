package obdtool.dg.agh.app.utils.enums;

public enum ConnectionState {
    NONE, CONNECTING_ELM, ELM_CONNECTED, CONNECTING_OBD, OBD_CONNECTED, FAILED
}
