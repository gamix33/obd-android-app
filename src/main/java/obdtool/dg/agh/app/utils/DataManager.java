package obdtool.dg.agh.app.utils;
import java.util.HashMap;
import java.util.UUID;

import obdtool.dg.agh.app.utils.enums.ConnectionState;
import obdtool.dg.agh.app.utils.enums.ObdParamType;
import obdtool.dg.agh.app.services.StorageService;

/**
 * Singleton
 * Przechowywuje i zarządza globalnymi danymi aplikacji (bieżące dane z ECU, dane z GPS, log,
 * dane użytkownika, etc.)
 */
public class DataManager {

    private static DataManager instance = new DataManager(); //\ instancja

    private static UUID authToken; //\ token autoryzacyjny API użytkownika
    private static UUID profileId; //\ bieżący profil użytkownika

    private static String logContent; //\ zawartość logu

    private static ConnectionState connectionState; //\ status połączenia z OBD
    private static boolean tripTrackingEnabled; //\ flaga, czy rejestrowanie trasy jest włączone

    //\ stałe używane w holderach
    public static final int MSG_STATE_CHANGED = 0; //\ zmiana statusu
    public static final int MSG_BLUETOOTH_CONNECTED = 1; //\ połączono z urządzeniem bluetooth
    public static final int MSG_UPDATE_PARAM = 2; //\ aktualizacja parametru z OBD
    public static final int MSG_LOCATION_CHANGED = 3; //\ zmiana współrzędnych geograficznych
    public static final int TROUBLE_CODES_UPDATE = 4;
    public static final int TROUBLE_CODES_DELETED = 5;

    private static HashMap<ObdParamType, ObdParam> currentParams; //\ aktualne parametry z OBD

    private static Double latitude; //\ szerokość geograficzna
    private static Double longitude; //\ długość geograficzna

    private static String troubleCodes;

    private static StorageService storingService; //\ serwis zapisujący dane w lokalnej bazie

    ////////////////////////////////////////////////////////////////////////////////////////////////

    //Konstruktor
    private DataManager(){
        authToken = null;//UUID.randomUUID();
        logContent = "";
        connectionState = ConnectionState.NONE;
        tripTrackingEnabled = false;
        currentParams = new HashMap<>();
        latitude = null;
        longitude = null;
        storingService = new StorageService();
        troubleCodes = null;
    }

    /**
     * Pobiera instancję DataManager'a
     * @return DataManager
     */
    public static DataManager getInstance() {
        return instance;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Dodaje wiadomość do logu
     * @param text - String wiadomość
     */
    public static void log(String text){
        logContent += "\n" + text;
        if(logContent.length() > 10000){
            logContent = logContent.substring(1000);
        }
    }

    /**
     * Zwraca zawartość logu
     * @return zawartość logu
     */
    public static String getLog(){
        return logContent;
    }

    public static UUID getAuthToken() {
        return authToken;
    }

    public static void setAuthToken(UUID authToken) {
        DataManager.authToken = authToken;
    }

    public static ConnectionState getConnectionState() {
        return connectionState;
    }

    public static void setConnectionState(ConnectionState state) {
        connectionState = state;

        if(connectionState == ConnectionState.OBD_CONNECTED && !storingService.isEnabled()){
            storingService.startStoring();
        } else if(connectionState != ConnectionState.OBD_CONNECTED && storingService.isEnabled() && !isTripTrackingEnabled()){
            storingService.stopStoring();
        }
    }

    public static Double getLatitude() {
        return latitude;
    }

    public static void setLatitude(Double latitude) {
        DataManager.latitude = latitude;

        log("latitude " + String.valueOf(latitude));
    }

    public static Double getLongitude() {
        return longitude;
    }

    public static void setLongitude(Double longitude) {
        DataManager.longitude = longitude;

        log("longitude " + String.valueOf(longitude));
    }

    public static ObdParam getParam(ObdParamType paramType) {
        return currentParams.containsKey(paramType) ? currentParams.get(paramType): new ObdParam();
    }

    public static void updateParam(ObdParamType paramType, ObdParam param) {
        currentParams.put(paramType, param);

        log(paramType.toString() + " " + param.getValue());
    }

    public static boolean isTripTrackingEnabled() {
        return tripTrackingEnabled;
    }

    public static void setTripTrackingEnabled(boolean tripTrackingEnabled) {
        DataManager.tripTrackingEnabled = tripTrackingEnabled;

        log("trip tracking status " + String.valueOf(tripTrackingEnabled));

        if(isTripTrackingEnabled() && !storingService.isEnabled()){
            storingService.startStoring();
        } else if(!isTripTrackingEnabled() && getConnectionState() != ConnectionState.OBD_CONNECTED && storingService.isEnabled()){
            storingService.stopStoring();
        }
    }

    public static String getTroubleCodes() {
        return troubleCodes;
    }

    public static void setTroubleCodes(String troubleCodes) {
        if(!troubleCodes.trim().equals("")){
            log("setting trouble code " + troubleCodes);
            DataManager.troubleCodes = troubleCodes;
        }
    }

    public static UUID getProfileId() {
        return profileId;
    }

    public static void setProfileId(UUID profileId) {
        DataManager.profileId = profileId;
    }
}
