package obdtool.dg.agh.app.utils.enums;

public enum EngineType {
    benzine, diesel, gas, hybrid
}
