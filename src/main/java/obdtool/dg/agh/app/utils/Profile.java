package obdtool.dg.agh.app.utils;


import java.util.UUID;

import obdtool.dg.agh.app.utils.enums.EngineType;

public class Profile {
    private UUID id;

    private String name;

    private Integer year;

    private EngineType type;

    private Integer capacity;

    public Profile(){}

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public EngineType getType() {
        return type;
    }

    public void setType(EngineType type) {
        this.type = type;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }
}
