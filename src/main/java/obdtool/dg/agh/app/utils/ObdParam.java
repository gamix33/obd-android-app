package obdtool.dg.agh.app.utils;

import com.github.pires.obd.commands.ObdCommand;

import obdtool.dg.agh.app.utils.enums.ObdParamType;

public class ObdParam {
    private ObdParamType type;
    private String value;
    private String unit;
    private ObdCommand command;

    public ObdParam(){
        this.type = null;
        this.unit = "";
        this.value = null;
        this.command = null;
    }

    public ObdParamType getType() {
        return type;
    }

    public void setType(ObdParamType type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public Integer getValueAsInteger(){
        Integer result = null;
        try{
            result = Integer.valueOf(value);
        }catch (Exception e){
            //DataManager.log(String.format("can not cast %s to Integer", value));
        }
        return result;
    }

    public Double getValueAsDouble(){
        Double result = null;
        try{
            result = Double.valueOf(value);
        }catch (Exception e){
            //DataManager.log(String.format("can not cast %s to Double", value));
        }
        return result;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public ObdCommand getCommand() {
        return command;
    }

    public void setCommand(ObdCommand command) {
        this.command = command;
    }
}
