package obdtool.dg.agh.app.view.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import obdtool.dg.agh.app.R;
import obdtool.dg.agh.app.utils.DataManager;
import obdtool.dg.agh.app.utils.enums.ObdParamType;
import obdtool.dg.agh.app.models.ObdData;

public class StatsFragment extends Fragment {

    private Spinner firstParamSpinner;
    private Spinner secondParamSpinner;
    private Spinner tripSpinner;
    private Button showChartButton;
    private HashMap<String, ObdParamType> items;
    private String currentProfileId;

    public StatsFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_stats, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        items = new HashMap<>();
        items.put(getString(R.string.engine_rpm), ObdParamType.RPM);
        items.put(getString(R.string.speed), ObdParamType.SPEED);
        items.put(getString(R.string.acu_voltage), ObdParamType.ACU_VOLTAGE);
        items.put(getString(R.string.oil_temp), ObdParamType.OIL_TEMP);
        items.put(getString(R.string.air_intake_temp), ObdParamType.INTAKE_TEMP);
        items.put(getString(R.string.coolant_temp), ObdParamType.ENGINE_COOLANT_TEMP);
        items.put(getString(R.string.throttle_position), ObdParamType.THROTTLE_POSITION);
        items.put(getString(R.string.fuel_consumption), ObdParamType.FUEL_CONSUMPTION);
        items.put(getString(R.string.fuel_level), ObdParamType.FUEL_LEVEL);

        currentProfileId = DataManager.getProfileId() != null ? DataManager.getProfileId().toString() : "";

        ArrayList<String> itemsArr = new ArrayList<>(items.keySet());
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), R.layout.row_list_item, itemsArr);

        firstParamSpinner = (Spinner) getView().findViewById(R.id.firstParamSpinner);
        firstParamSpinner.setAdapter(adapter);
        firstParamSpinner.setSelection(5, false);

        secondParamSpinner = (Spinner) getView().findViewById(R.id.secondParamSpinner);
        secondParamSpinner.setAdapter(adapter);
        secondParamSpinner.setSelection(7, false);

        List<ObdData> records = ObdData.findWithQuery(
                ObdData.class,
                "SELECT * FROM OBD_DATA WHERE PROFILE_ID = ? GROUP BY TRIP_NAME;",
                currentProfileId
        );
        ArrayList<String> tripsArr = new ArrayList<>();
        String tripName;
        for(ObdData record:records){
            tripName = record.getTripName();
            if(tripName != null) tripsArr.add(tripName);
        }
        ArrayAdapter<String> tripsAdapter = new ArrayAdapter<>(getContext(), R.layout.row_list_item, tripsArr);
        tripSpinner = (Spinner) getView().findViewById(R.id.tripSpinner);
        tripSpinner.setAdapter(tripsAdapter);

        showChartButton = (Button) getView().findViewById(R.id.showChartBtn);
        showChartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("first_param", items.get(firstParamSpinner.getSelectedItem().toString()).name());
                bundle.putString("second_param", items.get(secondParamSpinner.getSelectedItem().toString()).name());
                String tripName = (tripSpinner.getSelectedItem() != null) ? tripSpinner.getSelectedItem().toString() : "";
                bundle.putString("trip_name", tripName);

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                ChartFragment chartFragment = new ChartFragment();
                chartFragment.setArguments(bundle);
                fragmentManager.beginTransaction().replace(R.id.mainContent, chartFragment).addToBackStack("chart").commit();
            }
        });

    }
}

