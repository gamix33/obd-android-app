package obdtool.dg.agh.app.view.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

import obdtool.dg.agh.app.R;
import obdtool.dg.agh.app.core.ui.PromptDialog;
import obdtool.dg.agh.app.models.ObdData;
import obdtool.dg.agh.app.view.activities.MainActivity;

public class TripMapFragment extends Fragment implements OnMapReadyCallback {

    private MapView mapView;
    private GoogleMap map;

    Button backBtn;
    Button changeNameBtn;
    Button deleteBtn;

    private String tripName;

    public TripMapFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        tripName = getArguments().getString("trip_name");
        return inflater.inflate(R.layout.fragment_trip_map, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mapView = (MapView) view.findViewById(R.id.tripMap);

        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);

        backBtn = (Button) view.findViewById(R.id.backToTripsList);
        backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                backToTripsList();
            }
        });

        changeNameBtn = (Button) view.findViewById(R.id.changeTripName);
        changeNameBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                changeTripName();
            }
        });

        deleteBtn = (Button) view.findViewById(R.id.deleteTrip);
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                deleteTrip();
            }
        });

        ((MainActivity) getActivity()).changeBarText(tripName);
    }

    private void backToTripsList(){
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        TripsFragment tripsFragment = new TripsFragment();
        fragmentManager.beginTransaction().replace(R.id.mainContent, tripsFragment).addToBackStack("trips_list").commit();
    }

    private void changeTripName(){
        PromptDialog promptDialog = new PromptDialog(getContext(), R.string.tripNameChange, R.string.tripNameChangeInfo, tripName) {
            @Override
            public boolean onOkClicked(String input) {
                if(input.length() == 0)  return false;

                boolean exist = ObdData.find(ObdData.class, "TRIP_NAME = ?", input).size() > 0;
                if(!exist){
                    ObdData.executeQuery("UPDATE OBD_DATA SET TRIP_NAME = ? WHERE TRIP_NAME = ?;", input, tripName);
                    tripName = input;
                    ((MainActivity) getActivity()).changeBarText(tripName);
                    return true;
                }else{
                    return false;
                }
            }
        };
        promptDialog.show();
    }

    private void deleteTrip(){
        ObdData.executeQuery("DELETE FROM OBD_DATA WHERE TRIP_NAME = ?", tripName);
        backToTripsList();
    }

    @Override
    public void onMapReady(GoogleMap _map) {
        map = _map;

        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            public View getInfoWindow(Marker m) {
                return null;
            }

            public View getInfoContents(Marker m) {
                View v = getLayoutInflater(null).inflate(R.layout.infowindow, null);

                TextView content = (TextView) v.findViewById(R.id.content);
                content.setText(m.getSnippet());

                return v;
            }
        });

        drawTrip();
    }

    private void drawTrip(){
        List<ObdData> tripLocations = ObdData.find(ObdData.class, "TRIP_NAME = ?", tripName);

        if(tripLocations.size() > 1){
            PolylineOptions options = new PolylineOptions();

            options.color(ContextCompat.getColor(getContext(), R.color.colorPrimary));
            options.width(25);
            options.visible(true);

            LatLng centerLatlng = null;
            LatLng latlong;
            MarkerOptions marker;
            for(ObdData location: tripLocations){
                if(location.getLatitude() != null && location.getLongitude() != null){
                    latlong = new LatLng(location.getLatitude(), location.getLongitude());
                    options.add(latlong);
                    if(centerLatlng == null){
                        centerLatlng = latlong;
                    }

                    marker = new MarkerOptions().position(latlong).icon(BitmapDescriptorFactory.defaultMarker());
                    marker.snippet(location.toString());
                    map.addMarker(marker);
                }
            }

            if(centerLatlng != null){
                CameraUpdate center = CameraUpdateFactory.newLatLng(centerLatlng);
                map.moveCamera(center);
            }
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
            map.animateCamera(zoom);

            map.addPolyline(options);
        }
    }

}
