package obdtool.dg.agh.app.view.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import obdtool.dg.agh.app.R;
import obdtool.dg.agh.app.models.ObdData;
import obdtool.dg.agh.app.utils.DataManager;

public class TripsFragment extends Fragment {

    private ListView tripsList;
    private TextView noneInfo;
    private String currentProfileId;

    public TripsFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_trips, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tripsList = (ListView) getView().findViewById(R.id.tripsList);
        noneInfo = (TextView) getView().findViewById(R.id.noneTripsInfo);

        currentProfileId = DataManager.getProfileId() != null ? DataManager.getProfileId().toString() : "";

        fillList();
    }

    private void fillList(){
        List<ObdData> records = ObdData.findWithQuery(
                ObdData.class,
                "SELECT * FROM OBD_DATA WHERE PROFILE_ID = ? GROUP BY TRIP_NAME;",
                currentProfileId
        );
        System.out.println(currentProfileId);
        ArrayList<String> items = new ArrayList<>();

        String tripName;
        for(ObdData record: records){
            tripName = record.getTripName();
            if(tripName != null) items.add(tripName);
            System.out.println(record.getProfileId());
        }

        if(items.size() == 0){
            noneInfo.setVisibility(View.VISIBLE);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), R.layout.row_list_item, items);
        tripsList.setAdapter(adapter);
        tripsList.setClickable(true);
        tripsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                String tripName = tripsList.getItemAtPosition(position).toString();
                Bundle bundle = new Bundle();
                bundle.putString("trip_name", tripName);

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                TripMapFragment tripMapFragment = new TripMapFragment();
                tripMapFragment.setArguments(bundle);
                fragmentManager.beginTransaction().replace(R.id.mainContent, tripMapFragment).addToBackStack("map").commit();
            }
        });
    }
}
