package obdtool.dg.agh.app.view.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import obdtool.dg.agh.app.R;
import obdtool.dg.agh.app.core.rest.ApiResponse;
import obdtool.dg.agh.app.core.rest.RestClient;
import obdtool.dg.agh.app.utils.DataManager;
import obdtool.dg.agh.app.utils.Profile;

public class ProfilesFragment extends Fragment {

    protected ProfilesTask profilesTask;
    protected List<Profile> profiles;
    protected RadioGroup profilesRadioGroup;

    public ProfilesFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profiles, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        profilesTask = null;
        profilesRadioGroup = (RadioGroup) getView().findViewById(R.id.profilesRadioGroup);
        profilesRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                View radioButton = profilesRadioGroup.findViewById(checkedId);
                int index = profilesRadioGroup.indexOfChild(radioButton);
                UUID chceckedProfileId = profiles.get(index).getId();
                DataManager.setProfileId(chceckedProfileId);
            }
        });

        fetchProfiles();
    }

    public void fetchProfiles(){
        if(profilesTask != null){
            return;
        }

        profiles = new ArrayList<>();
        profilesTask = new ProfilesTask();
        profilesTask.execute((Void) null);
    }

    protected void renderProfiles(){
        RadioButton radioButton;
        for(Profile profile : profiles){
            radioButton = new RadioButton(getContext());
            radioButton.setTextSize(16);
            radioButton.setText(profile.getName());
            profilesRadioGroup.addView(radioButton);

            if(DataManager.getProfileId().equals(profile.getId())){
                radioButton.setChecked(true);
            }
        }
    }

    protected void showMessage(String message){
        Toast toast = Toast.makeText(getContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }

    public class ProfilesTask extends AsyncTask<Void, Void, Boolean> {
        private ApiResponse response;

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                response = RestClient.get("profiles");
                return response.getCode() == 200;
            } catch (IOException e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            profilesTask = null;

            if(success){
                Gson gson = new Gson();
                JsonArray data = response.getBodyArray();
                for (JsonElement record : data) {
                    profiles.add(gson.fromJson(record.getAsJsonObject(), Profile.class));
                }
                renderProfiles();
            } else {
                showMessage(response.getErrorMessage());
            }
        }

        @Override
        protected void onCancelled() {
            profilesTask = null;
        }
    }
}
