package obdtool.dg.agh.app.view.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import java.io.IOException;
import java.util.List;

import obdtool.dg.agh.app.R;
import obdtool.dg.agh.app.core.rest.ApiResponse;
import obdtool.dg.agh.app.core.App;
import obdtool.dg.agh.app.core.rest.RestClient;
import obdtool.dg.agh.app.core.ui.DrawerItem;
import obdtool.dg.agh.app.core.ui.DrawerItemCustomAdapter;
import obdtool.dg.agh.app.models.Config;
import obdtool.dg.agh.app.models.ObdData;
import obdtool.dg.agh.app.utils.DataManager;
import obdtool.dg.agh.app.utils.enums.ConnectionState;
import obdtool.dg.agh.app.view.fragments.DashboardFragment;
import obdtool.dg.agh.app.view.fragments.FaultsFragment;
import obdtool.dg.agh.app.view.fragments.LogFragment;
import obdtool.dg.agh.app.view.fragments.ProfilesFragment;
import obdtool.dg.agh.app.view.fragments.StatsFragment;
import obdtool.dg.agh.app.view.fragments.TripsFragment;

import static obdtool.dg.agh.app.utils.Helpers.isOnline;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private ListView drawerList;
    private ActionBarDrawerToggle drawerToggle;
    private static DrawerItem[] drawerItems;
    private Fragment activeFragment = null;
    private LogoutTask logoutTask;
    private SyncTask syncTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        onCreateDrawer();
    }

    protected void onCreateDrawer(){
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_reorder);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerList = (ListView) findViewById(R.id.left_drawer);

        drawerLayout.setDrawerListener(drawerToggle);

        drawerItems = new DrawerItem[5];
        drawerItems[0] = new DrawerItem(R.drawable.ic_dashboard, App.getContext().getString(R.string.dashboard));
        drawerItems[1] = new DrawerItem(R.drawable.ic_error,  App.getContext().getString(R.string.fault));
        drawerItems[2] = new DrawerItem(R.drawable.ic_chart,  App.getContext().getString(R.string.stats));
        drawerItems[3] = new DrawerItem(R.drawable.ic_map,  App.getContext().getString(R.string.trips));
        drawerItems[4] = new DrawerItem(R.drawable.ic_log,  App.getContext().getString(R.string.log));

        drawerList.setAdapter(new DrawerItemCustomAdapter(this, R.layout.row_drawer_item, drawerItems));

        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                changeActivity(position);

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });

        if(activeFragment == null){
            changeActivity(0);
        }
    }

    private void changeActivity(int position){
        FragmentManager fragmentManager = getSupportFragmentManager();
        String fragmentName = drawerItems[position].getName();
        Fragment fragment = null;

        switch(position){
            case 0:
                fragment  = new DashboardFragment();
                break;
            case 1:
                fragment  = new FaultsFragment();
                break;
            case 2:
                fragment  = new StatsFragment();
                break;
            case 3:
                fragment  = new TripsFragment();
                break;
            case 4:
                fragment  = new LogFragment();
                break;
        }

        fragmentManager.beginTransaction().replace(R.id.mainContent, fragment).addToBackStack(fragmentName).commit();

        drawerList.setItemChecked(position, true);
        drawerList.setSelection(position);
        changeBarText(fragmentName);
    }

    public void changeBarText(String text){
        getSupportActionBar().setTitle(text);
    }

    private void toggleMenu() {
        if (drawerLayout.isDrawerVisible(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            drawerLayout.openDrawer(GravityCompat.START);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                toggleMenu();
                return true;
            case R.id.profiles:
                openProfilesFragment();
                return true;
            case R.id.sync:
                sync();
                return true;
            case R.id.logout:
                logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void showMessage(String message){
        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }

    protected void openProfilesFragment(){
        drawerLayout.closeDrawer(GravityCompat.START);
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = new ProfilesFragment();
        fragmentManager.beginTransaction().replace(R.id.mainContent, fragment).addToBackStack("profilesFragment").commit();

        changeBarText(getString(R.string.profiles));
    }

    protected void logout(){
        if(logoutTask != null){
            return;
        }

        drawerLayout.closeDrawer(GravityCompat.START);

        logoutTask = new LogoutTask();
        logoutTask.execute((Void) null);
    }

    protected void openLoginActivity(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public class LogoutTask extends AsyncTask<Void, Void, Boolean> {
        private ApiResponse response;

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                response = RestClient.delete("users/logout");
                return response.getCode() == 200;
            } catch (IOException e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            logoutTask = null;

            if(success){
                DataManager.setAuthToken(null);
                DataManager.setProfileId(null);
                Config.clear();

                openLoginActivity();
            } else {
                showMessage(response.getErrorMessage());
            }
        }

        @Override
        protected void onCancelled() {
            logoutTask = null;
        }
    }

    private void sync(){
        if(!isOnline()){
            Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.offline_error), Toast.LENGTH_SHORT);
            toast.show();
            return;
        }

        if(!DataManager.getConnectionState().equals(ConnectionState.NONE) &&
                !DataManager.getConnectionState().equals(ConnectionState.FAILED)){
            Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.disconnect_obd_before_sync), Toast.LENGTH_SHORT);
            toast.show();

        }else{
            syncTask = new SyncTask();
            syncTask.execute((Void) null);
        }
    }

    public class SyncTask extends AsyncTask<Void, Void, Boolean> {
        private ApiResponse response;

        private Long getLastDataTimestamp(){
            String profileIdStr = DataManager.getProfileId().toString();
            List<ObdData> data  = ObdData.findWithQuery(
                    ObdData.class,
                    "SELECT * FROM OBD_DATA WHERE PROFILE_ID = ? ORDER BY TIME DESC LIMIT 1;",
                    profileIdStr);
            return data.size() > 0 ? data.get(0).getTime() : 0L;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                String profileId = DataManager.getProfileId().toString();
                response = RestClient.get(String.format("synchronization/%s/check", profileId));
                if(response.getCode() != 200) {
                    return false;
                };

                Long lastLocalTime = getLastDataTimestamp();
                Long lastRemoteTime = response.getBodyObject().get("data").getAsLong();

                while( !lastLocalTime.equals(lastRemoteTime) ) {
                    if (lastRemoteTime.equals(0L) || (lastLocalTime > lastRemoteTime)) {
                        //jeśli dane na urządzeniu są nowsze
                        List<ObdData> data = ObdData.find(
                                ObdData.class,
                                "PROFILE_ID = ? AND TIME > ? LIMIT 50",
                                profileId, lastRemoteTime.toString()
                        );
                        response = RestClient.post(String.format("synchronization/%s/upload", profileId), data);
                        lastRemoteTime = response.getBodyObject().get("data").getAsLong();
                    } else if (lastLocalTime.equals(0L) || (lastRemoteTime > lastLocalTime)) {
                        //jeśli dane w chmurze są nowsze
                        String lastLocalStr = lastLocalTime != null ? lastLocalTime.toString() : "0";
                        response = RestClient.get(String.format("synchronization/%s/download/%s", profileId, lastLocalStr));
                        Gson gson = new Gson();
                        JsonArray data = response.getBodyArray();
                        for (JsonElement record : data) {
                            ObdData.save(gson.fromJson(record.getAsJsonObject(), ObdData.class));
                        }
                        lastLocalTime = getLastDataTimestamp();
                    }
                }

                return true;
            } catch (IOException e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            syncTask = null;

            if(success){
                showMessage(getApplicationContext().getString(R.string.synchronization_complete));
            } else {
                showMessage(response.getErrorMessage());
            }
        }

        @Override
        protected void onCancelled() {
            syncTask = null;
        }
    }

}
