package obdtool.dg.agh.app.view.fragments;


import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Set;

import obdtool.dg.agh.app.R;
import obdtool.dg.agh.app.utils.DataManager;
import obdtool.dg.agh.app.utils.enums.ConnectionState;
import obdtool.dg.agh.app.utils.ObdParam;
import obdtool.dg.agh.app.services.BluetoothService;
import obdtool.dg.agh.app.services.GpsService;
import obdtool.dg.agh.app.services.ObdReaderService;

import static obdtool.dg.agh.app.utils.DataManager.MSG_BLUETOOTH_CONNECTED;
import static obdtool.dg.agh.app.utils.DataManager.MSG_LOCATION_CHANGED;
import static obdtool.dg.agh.app.utils.DataManager.MSG_STATE_CHANGED;
import static obdtool.dg.agh.app.utils.DataManager.MSG_UPDATE_PARAM;

public class DashboardFragment extends Fragment {

    private BluetoothService bluetoothService;
    private GpsService gpsService;
    private ObdReaderService obdReaderService;
    private BluetoothDevice device;

    private Handler handler;

    private Button connectBtn;
    private Button tripBtn;
    private TextView stateTextView;
    private TextView gpsStatus;
    private TextView latitudeValue;
    private TextView longitudeValue;

    private boolean troubleCodesInformed;

    public DashboardFragment() {}

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        connectBtn = (Button) getView().findViewById(R.id.connectBtn);
        tripBtn = (Button) getView().findViewById(R.id.tripBtn);
        stateTextView = (TextView) getView().findViewById(R.id.stateValue);
        gpsStatus = (TextView) getView().findViewById(R.id.gpsStatus);

        latitudeValue = (TextView) getView().findViewById(R.id.latitudeValue);
        longitudeValue = (TextView) getView().findViewById(R.id.longitudeValue);

        troubleCodesInformed = false;

        handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);

                switch (msg.what){
                    case MSG_STATE_CHANGED:
                        refreshConnectState();

                        switch (DataManager.getConnectionState()){
                            case NONE:
                                stateTextView.setText(R.string.not_connected);
                                break;
                            case CONNECTING_ELM:
                                stateTextView.setText(R.string.connecting_elm);
                                break;
                            case ELM_CONNECTED:
                                stateTextView.setText(R.string.elm_conected);
                                break;
                            case CONNECTING_OBD:
                                stateTextView.setText(R.string.connecting_obd);
                                break;
                            case OBD_CONNECTED:
                                stateTextView.setText(R.string.obd_conected);
                                break;
                            case FAILED:
                                stateTextView.setText(R.string.not_connected);
                                Toast toast = Toast.makeText(getContext(), getString(R.string.connection_error), Toast.LENGTH_SHORT);
                                toast.show();
                                break;
                        }
                        break;
                    case MSG_UPDATE_PARAM:
                        onUpdateParam((ObdParam) msg.obj);
                        break;
                    case MSG_BLUETOOTH_CONNECTED:
                        startObdService((BluetoothSocket) msg.obj);
                        break;
                    case MSG_LOCATION_CHANGED:
                        onLocationChange((Location) msg.obj);
                        break;
                }
            }

        };

        bluetoothService = new BluetoothService(handler);
        connectBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(DataManager.getConnectionState().equals(ConnectionState.NONE) || DataManager.getConnectionState().equals(ConnectionState.FAILED)){
                    if(!bluetoothService.isConnected()){
                        showDevicesDialog();
                    } else {
                        BluetoothSocket socket = bluetoothService.getSocket();
                        startObdService(socket);
                    }
                } else {
                    obdReaderService.stop();
                    bluetoothService.disconnect();
                }
                refreshConnectState();
            }
        });

        if(DataManager.getConnectionState().equals(ConnectionState.ELM_CONNECTED)){
            BluetoothSocket socket = bluetoothService.getSocket();
            startObdService(socket);
        }

        gpsService = new GpsService(getContext(), handler);
        tripBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(!DataManager.isTripTrackingEnabled()){
                    startGPS();
                } else {
                    stopGPS();
                }
            }
        });

        refreshConnectState();
        refreshTripTrackingState();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    private void startGPS(){
        boolean permissionGranted = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        if(permissionGranted){
            gpsService.startGps();
            refreshTripTrackingState();
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 200);
        }
    }

    private void stopGPS(){
        gpsService.stopGps();
        refreshTripTrackingState();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch(requestCode){
            case 200: {
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    gpsService.startGps();
                    refreshTripTrackingState();
                }
            }
        }
    }

    private void onLocationChange(Location location){
        latitudeValue.setText(String.valueOf(location.getLatitude()));
        longitudeValue.setText(String.valueOf(location.getLongitude()));
    }

    private void refreshConnectState(){
        boolean disconnected = DataManager.getConnectionState().equals(ConnectionState.NONE) || DataManager.getConnectionState().equals(ConnectionState.FAILED);
        if(disconnected){
            connectBtn.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
            connectBtn.setText(R.string.obd_connect);

        } else {
            connectBtn.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.darkred));
            connectBtn.setText(R.string.obd_disconnect);
        }
    }

    private void refreshTripTrackingState(){
        if(DataManager.isTripTrackingEnabled()){
            tripBtn.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.darkred));
            tripBtn.setText(R.string.stop_trip);
            gpsStatus.setText(R.string.enabled);
            latitudeValue.setText(String.valueOf(DataManager.getLatitude()));
            longitudeValue.setText(String.valueOf(DataManager.getLongitude()));
        } else {
            tripBtn.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.green));
            tripBtn.setText(R.string.start_trip);
            gpsStatus.setText(R.string.disabled);
        }
    }

    private void showDevicesDialog() {
        final ArrayList devices = new ArrayList();
        ArrayList devicesNames = new ArrayList();

        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();

        if (btAdapter == null) {
            Toast toast = Toast.makeText(getContext(), getString(R.string.bluetooth_not_supported), Toast.LENGTH_SHORT);
            toast.show();
            return;
        } else if (!btAdapter.isEnabled()) {

            Toast toast = Toast.makeText(getContext(), getString(R.string.turn_on_bluetooth), Toast.LENGTH_SHORT);
            toast.show();
            return;
        }

        Set<BluetoothDevice> bondedDevices = btAdapter.getBondedDevices();
        if (bondedDevices.size() > 0) {
            for (BluetoothDevice device : bondedDevices) {
                devicesNames.add(device.getName() + "\n" + device.getAddress());
                devices.add(device.getAddress());
            }
        }

        final AlertDialog.Builder devicesDialog = new AlertDialog.Builder(getContext());

        ArrayAdapter adapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_singlechoice,
                devicesNames.toArray(new String[devicesNames.size()]));

        AlertDialog.Builder builder = devicesDialog.setSingleChoiceItems(adapter, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                int selectedIndex = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                final String deviceId = devices.get(selectedIndex).toString();
                BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
                device = btAdapter.getRemoteDevice(deviceId);
                bluetoothService.connect(device);
            }
        });

        devicesDialog.setTitle(getString(R.string.choose_device));
        devicesDialog.show();
    }

    private void startObdService(BluetoothSocket socket) {
        if(obdReaderService == null){
            obdReaderService = new ObdReaderService(handler);
        } else {
            obdReaderService.stop();
        }
        obdReaderService.startRead(socket);
    }

    public void onUpdateParam(ObdParam obdParam){
        if(getView() == null){
            return;
        }

        TextView paramTextView = null;

        switch (obdParam.getType()){
            case SPEED:
                paramTextView = (TextView) getView().findViewById(R.id.speedValue);
                break;
            case RPM:
                paramTextView = (TextView) getView().findViewById(R.id.rpmValue);
                break;
            case INTAKE_TEMP:
                paramTextView = (TextView) getView().findViewById(R.id.aiTempValue);
                break;
            case ACU_VOLTAGE:
                paramTextView = (TextView) getView().findViewById(R.id.acuVoltageValue);
                break;
            case ENGINE_COOLANT_TEMP:
                paramTextView = (TextView) getView().findViewById(R.id.coolantTempValue);
                break;
            case OIL_TEMP:
                paramTextView = (TextView) getView().findViewById(R.id.oilTempValue);
                break;
            case THROTTLE_POSITION:
                paramTextView = (TextView) getView().findViewById(R.id.throttlePositionValue);
                break;
            case FUEL_LEVEL:
                paramTextView = (TextView) getView().findViewById(R.id.fuelLevelValue);
                break;
            case FUEL_CONSUMPTION:
                paramTextView = (TextView) getView().findViewById(R.id.fuelConsumptionValue);
                break;
            case TROUBLE_CODES:
                if(!troubleCodesInformed) {
                    DataManager.setTroubleCodes(obdParam.getValue());
                    if(DataManager.getTroubleCodes() != null){
                        Toast toast = Toast.makeText(getContext(), getString(R.string.trouble_codes_detected), Toast.LENGTH_LONG);
                        toast.show();
                        troubleCodesInformed = true;
                    }
                }
        }

        if(paramTextView != null){
            String valueStr = obdParam.getValue() != null ? obdParam.getValue() + obdParam.getUnit() : getString(R.string.na);
            paramTextView.setText(valueStr);
        }
    }

}
