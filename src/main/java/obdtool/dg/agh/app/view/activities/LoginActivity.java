package obdtool.dg.agh.app.view.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import android.os.AsyncTask;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonArray;

import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

import obdtool.dg.agh.app.R;
import obdtool.dg.agh.app.core.rest.RestClient;
import obdtool.dg.agh.app.models.Config;
import obdtool.dg.agh.app.utils.DataManager;
import obdtool.dg.agh.app.core.rest.ApiResponse;

import static obdtool.dg.agh.app.utils.Helpers.isOnline;

public class LoginActivity extends AppCompatActivity {

    private SignTask signTask = null;

    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        /*if(Config.get() != null){
            AuthTask authTask = new AuthTask();
            authTask.execute((Void) null);
            return;
        }*/

        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);

        Button loginBtn = (Button) findViewById(R.id.loginBtn);
        loginBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                sign(false);
            }
        });

        Button registerBtn = (Button) findViewById(R.id.registerBtn);
        registerBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                sign(true);
            }
        });
    }

    private void sign(boolean register) {

        if (signTask != null) {
            return;
        }
        
        if(!isOnline()){
            Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.offline_error), Toast.LENGTH_SHORT);
            toast.show();
            return;
        }

        mEmailView.setError(null);
        mPasswordView.setError(null);

        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_input_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if(register && password.length() < 8){
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            signTask = new SignTask(email, password, register);
            signTask.execute((Void) null);
        }
    }

    private void showMessage(String message){
        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }

    private void authSuccess(UUID authToken, JsonArray profiles){
        DataManager.setAuthToken(authToken);
        UUID profileId = UUID.fromString(profiles.get(0).getAsString());
        DataManager.setProfileId(profileId);

        Config config = new Config(authToken, profileId);
        config.save();

        openMainActivity();
    }

    private void openMainActivity(){
        if(mPasswordView != null){
            mPasswordView.setText("");
        }

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    public class AuthTask extends AsyncTask<Void, Void, Boolean> {
        private ApiResponse response;

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                response = RestClient.get("users/auth");
                return response.getCode() == 200;
            } catch (IOException e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            if (success) {
                openMainActivity();
            } else {
                DataManager.setAuthToken(null);
                DataManager.setProfileId(null);
            }
        }
    }

    public class SignTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;
        private boolean mRegister;
        private String action;
        private ApiResponse response;

        SignTask(String email, String password, boolean register) {
            mEmail = email;
            mPassword = password;
            mRegister = register;
            action = register ? "register" : "login";
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            HashMap<String, Object> data = new HashMap<>();
            data.put("email", mEmail);
            data.put("password", mPassword);
            try {
                response = RestClient.post("users/" + action, data);
                return response.getCode() == 200;
            } catch (IOException e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            signTask = null;

            if (success) {
                UUID token = UUID.fromString(response.getBodyObject().get("token").getAsString());
                JsonArray profiles = response.getBodyObject().get("profiles").getAsJsonArray();
                if(mRegister){
                    showMessage(getApplicationContext().getString(R.string.register_successful));
                }
                authSuccess(token, profiles);
            } else {
                showMessage(response.getErrorMessage());
            }
        }

        @Override
        protected void onCancelled() {
            signTask = null;
        }
    }
}

