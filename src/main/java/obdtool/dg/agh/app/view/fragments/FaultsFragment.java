package obdtool.dg.agh.app.view.fragments;


import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import obdtool.dg.agh.app.R;
import obdtool.dg.agh.app.utils.DataManager;
import obdtool.dg.agh.app.utils.enums.ConnectionState;
import obdtool.dg.agh.app.services.BluetoothService;
import obdtool.dg.agh.app.services.ObdReaderService;

import static obdtool.dg.agh.app.utils.DataManager.MSG_BLUETOOTH_CONNECTED;
import static obdtool.dg.agh.app.utils.DataManager.MSG_STATE_CHANGED;
import static obdtool.dg.agh.app.utils.DataManager.TROUBLE_CODES_DELETED;
import static obdtool.dg.agh.app.utils.DataManager.TROUBLE_CODES_UPDATE;

public class FaultsFragment extends Fragment {

    private Button checkFaults;
    private Button deleteFaults;
    private TextView troubleCodesView;

    private BluetoothService bluetoothService;
    private ObdReaderService obdReaderService;
    private Handler handler;

    public FaultsFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_faults, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        checkFaults = (Button) getView().findViewById(R.id.checkFaults);
        deleteFaults = (Button) getView().findViewById(R.id.deleteFaults);
        troubleCodesView = (TextView) getView().findViewById(R.id.troubleCodes);

        handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                Toast toast;

                switch (msg.what){
                    case TROUBLE_CODES_UPDATE:
                        fetchTroubleCodes();
                        break;
                    case TROUBLE_CODES_DELETED:
                        toast = Toast.makeText(getContext(), getString(R.string.trouble_codes_deleted), Toast.LENGTH_SHORT);
                        toast.show();
                        break;
                    case MSG_BLUETOOTH_CONNECTED:
                        startObdService((BluetoothSocket) msg.obj);
                        break;
                    case MSG_STATE_CHANGED:
                        if (DataManager.getConnectionState().equals(ConnectionState.FAILED)){
                            toast = Toast.makeText(getContext(), getString(R.string.connection_error), Toast.LENGTH_SHORT);
                            toast.show();
                        }
                        break;
                }
            }

        };

        bluetoothService = new BluetoothService(handler);
        checkFaults.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!bluetoothService.isConnected()){
                    showDevicesDialog();
                }
            }
        });

        deleteFaults.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obdReaderService.cleanTroubleCodes();
            }
        });

        if(bluetoothService.isConnected()){
            fetchTroubleCodes();
        }
    }


    private void showDevicesDialog() {
        final ArrayList devices = new ArrayList();
        ArrayList devicesNames = new ArrayList();

        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();

        if (btAdapter == null) {
            Toast toast = Toast.makeText(getContext(), getString(R.string.bluetooth_not_supported), Toast.LENGTH_SHORT);
            toast.show();
            return;
        } else if (!btAdapter.isEnabled()) {

            Toast toast = Toast.makeText(getContext(), getString(R.string.turn_on_bluetooth), Toast.LENGTH_SHORT);
            toast.show();
            return;
        }

        Set<BluetoothDevice> bondedDevices = btAdapter.getBondedDevices();
        if (bondedDevices.size() > 0) {
            for (BluetoothDevice device : bondedDevices) {
                devicesNames.add(device.getName() + "\n" + device.getAddress());
                devices.add(device.getAddress());
            }
        }

        final AlertDialog.Builder devicesDialog = new AlertDialog.Builder(getContext());

        ArrayAdapter adapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_singlechoice,
                devicesNames.toArray(new String[devicesNames.size()]));

        AlertDialog.Builder builder = devicesDialog.setSingleChoiceItems(adapter, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                int selectedIndex = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                final String deviceId = devices.get(selectedIndex).toString();
                BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
                BluetoothDevice device = btAdapter.getRemoteDevice(deviceId);

                bluetoothService = new BluetoothService(handler);
                bluetoothService.connect(device);
            }
        });

        devicesDialog.setTitle(getString(R.string.choose_device));
        devicesDialog.show();
    }

    private void startObdService(BluetoothSocket socket) {
        Toast toast = Toast.makeText(getContext(), getString(R.string.getting_trouble_codes), Toast.LENGTH_SHORT);
        toast.show();
        obdReaderService = new ObdReaderService(handler);
        obdReaderService.readTroubleCodes(socket);
    }

    private Map<String, String> getTroubleCodesDesc() {
        String[] keys = getResources().getStringArray(R.array.dtc_keys);
        String[] values = getResources().getStringArray(R.array.dtc_values);

        Map<String, String> result = new HashMap<>();
        for (int i = 0; i < keys.length; i++) {
            result.put(keys[i], values[i]);
        }

        return result;
    }

    private void fetchTroubleCodes(){
        String troubleCodesStr = DataManager.getTroubleCodes();

        if(troubleCodesStr != null){
            Map<String, String> codesDescriptions = getTroubleCodesDesc();

            String troubleCodesInfo = "";
            for (String code : troubleCodesStr.split("\n")) {
                troubleCodesInfo += code + " : " + codesDescriptions.get(code) + "\n";
                DataManager.log(code + " : " + codesDescriptions.get(code));
            }

            troubleCodesView.setText(troubleCodesInfo);
            deleteFaults.setVisibility(View.VISIBLE);
        }else{
            troubleCodesView.setText(R.string.none_faults);
            deleteFaults.setVisibility(View.GONE);
        }
        troubleCodesView.setVisibility(View.VISIBLE);
        checkFaults.setVisibility(View.GONE);

        DataManager.setConnectionState(ConnectionState.NONE);
    }
}
