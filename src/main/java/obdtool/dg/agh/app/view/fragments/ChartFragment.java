package obdtool.dg.agh.app.view.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.List;

import obdtool.dg.agh.app.R;
import obdtool.dg.agh.app.utils.enums.ObdParamType;
import obdtool.dg.agh.app.models.ObdData;
import obdtool.dg.agh.app.view.activities.MainActivity;


public class ChartFragment extends Fragment {

    private ObdParamType firstParamType;
    private ObdParamType secondParamType;
    private String tripName;
    private LineChart lineChart;

    public ChartFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        firstParamType = ObdParamType.valueOf(getArguments().getString("first_param"));
        secondParamType = ObdParamType.valueOf(getArguments().getString("second_param"));
        tripName = getArguments().getString("trip_name");

        return inflater.inflate(R.layout.fragment_chart, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lineChart = (LineChart) view.findViewById(R.id.lineChart);
        lineChart.getDescription().setEnabled(false);
        lineChart.animateY(500);

        ((MainActivity) getActivity()).changeBarText(String.format("%s & %s", firstParamType.name(), secondParamType.name()));
        drawChart();
    }

    private void drawChart(){
        List<ObdData> records = ObdData.find(ObdData.class, "TRIP_NAME = ?", tripName);

        if(records.size() == 0){
            return;
        }

        ArrayList<Entry> entries1 = new ArrayList<>();
        ArrayList<Entry> entries2 = new ArrayList<>();
        int i = 0;
        Float val1, val2;
        for(ObdData record: records){
            if (record != null){
                val1 = record.getParam(firstParamType);
                if(val1 != null){
                    entries1.add(new Entry(i, val1.floatValue()));
                }
                val2 = record.getParam(secondParamType);
                if(val2 != null){
                    entries2.add(new Entry(i, val2.floatValue()));
                }
            }
            ++i;
            if(i > 500)  break;
        }

        if(entries1.size() == 0 && entries2.size() == 0){
            return;
        }

        LineData lineData = new LineData();

        if(entries1.size() > 0) {
            LineDataSet dataSet1 = new LineDataSet(entries1, firstParamType.name());
            dataSet1.setColor(Color.GREEN);
            dataSet1.setCircleColor(Color.GREEN);
            dataSet1.setLineWidth(2);
            dataSet1.setValueTextSize(12);
            dataSet1.setAxisDependency(YAxis.AxisDependency.LEFT);
            lineData.addDataSet(dataSet1);
        }


        if(entries2.size() > 0) {
            LineDataSet dataSet2 = new LineDataSet(entries2, secondParamType.name());
            dataSet2.setColor(Color.RED);
            dataSet2.setCircleColor(Color.RED);
            dataSet2.setLineWidth(2);
            dataSet2.setValueTextSize(12);
            dataSet2.setAxisDependency(YAxis.AxisDependency.RIGHT);
            lineData.addDataSet(dataSet2);
        }

        lineChart.setData(lineData);
    }

}
