package obdtool.dg.agh.app.view.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import obdtool.dg.agh.app.R;
import obdtool.dg.agh.app.utils.DataManager;

public class LogFragment extends Fragment {

    public LogFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_log, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final TextView logOutput = (TextView) getView().findViewById(R.id.logOutput);
        logOutput.setText(DataManager.getLog());
    }
}
