package obdtool.dg.agh.app.models;

import com.orm.SugarRecord;

import java.util.List;
import java.util.UUID;


/**
 * Created by Damian on 03.12.2016.
 */

public class Config extends SugarRecord {
    private String authToken;
    private String currentProfileId;

    public Config(UUID authToken, UUID currentProfileId) {
        this.authToken = authToken.toString();
        this.currentProfileId = currentProfileId.toString();
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getCurrentProfileId() {
        return currentProfileId;
    }

    public void setCurrentProfileId(String currentProfileId) {
        this.currentProfileId = currentProfileId;
    }

    @Override
    public long save(){
        clear();
        return super.save();
    }

    public static void clear(){
        Config.executeQuery("DELETE FROM CONFIG;");
    }

    public static Config get(){
        List<Config> configs = Config.findWithQuery(Config.class, "SELECT * FROM CONFIG;");
        return configs.size() > 0 ? configs.get(0) : null;
    }
}

