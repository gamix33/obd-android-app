package obdtool.dg.agh.app.models;

import com.orm.SugarRecord;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import obdtool.dg.agh.app.utils.enums.ObdParamType;

public class ObdData extends SugarRecord {
    private Double latitude;
    private Double longitude;
    private String tripName;
    private long time;
    private Integer rpm;
    private Double speed;
    private Double acuVoltage;
    private Double airIntakeTemperature;
    private Double engineCoolantTemperature;
    private Double oilTemperature;
    private Double throttlePosition;
    private Double fuelLevel;
    private Double fuelConsumption;
    private String profileId; //UUID

    public ObdData(){
        this.time = System.currentTimeMillis() / 1000L;
    }

    public ObdData(Double latitude, Double longitude, String tripName, Integer rpm, Double speed, Double acuVoltage, Double airIntakeTemperature, Double engineCoolantTemperature, Double oilTemperature, Double throttlePosition, Double fuelLevel, Double fuelConsumption,  UUID profileId) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.tripName = tripName;
        this.rpm = rpm;
        this.speed = speed;
        this.acuVoltage = acuVoltage;
        this.airIntakeTemperature = airIntakeTemperature;
        this.engineCoolantTemperature = engineCoolantTemperature;
        this.oilTemperature = oilTemperature;
        this.throttlePosition = throttlePosition;
        this.fuelLevel = fuelLevel;
        this.fuelConsumption = fuelConsumption;
        this.profileId = profileId.toString();
        this.time = System.currentTimeMillis() / 1000L;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getTripName() {
        return tripName;
    }

    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public Integer getRpm() {
        return rpm;
    }

    public void setRpm(Integer rpm) {
        this.rpm = rpm;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Double getAcuVoltage() {
        return acuVoltage;
    }

    public void setAcuVoltage(Double acuVoltage) {
        this.acuVoltage = acuVoltage;
    }

    public Double getAirIntakeTemperature() {
        return airIntakeTemperature;
    }

    public void setAirIntakeTemperature(Double airIntakeTemperature) {
        this.airIntakeTemperature = airIntakeTemperature;
    }

    public Double getEngineCoolantTemperature() {
        return engineCoolantTemperature;
    }

    public void setEngineCoolantTemperature(Double engineCoolantTemperature) {
        this.engineCoolantTemperature = engineCoolantTemperature;
    }

    public Double getOilTemperature() {
        return oilTemperature;
    }

    public void setOilTemperature(Double oilTemperature) {
        this.oilTemperature = oilTemperature;
    }

    public Double getThrottlePosition() {
        return throttlePosition;
    }

    public void setThrottlePosition(Double throttlePosition) {
        this.throttlePosition = throttlePosition;
    }

    public Double getFuelLevel() {
        return fuelLevel;
    }

    public void setFuelLevel(Double fuelLevel) {
        this.fuelLevel = fuelLevel;
    }

    public Double getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(Double fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public Float getParam(ObdParamType paramType){
        Float result = null;
        Object pom = null;
        
        switch (paramType){
            case RPM:
                pom = this.rpm;
                break;
            case SPEED:
                pom = this.speed;
                break;
            case ACU_VOLTAGE:
                pom = this.acuVoltage;
                break;
            case INTAKE_TEMP:
                pom = this.airIntakeTemperature;
                break;
            case ENGINE_COOLANT_TEMP:
                pom = this.engineCoolantTemperature;
                break;
            case OIL_TEMP:
                pom = this.oilTemperature;
                break;
            case THROTTLE_POSITION:
                pom = this.throttlePosition;
                break;
            case FUEL_LEVEL:
                pom = this.fuelLevel;
                break;
            case FUEL_CONSUMPTION:
                pom = this.fuelConsumption;
                break;
        }

        if(pom != null){
            try{
                result = ((Double)pom).floatValue();
            }catch (ClassCastException e1){
                try{
                    result = ((Integer)pom).floatValue();
                }catch(ClassCastException e2){
                    //DataManager.log("can not cast for chart");
                }
            }
        }

        return result;
    }

    public String toString(){
        String result = "";

        if(this.getRpm() != null)  result += "RPM: " + this.getRpm() + "\n";
        if(this.getSpeed() != null)  result += "SPEED: " + this.getSpeed() + "km/h\n";
        if(this.getAcuVoltage() != null)  result += "ACU VOLTAGE: " + this.getAcuVoltage() + "V\n";
        if(this.getAirIntakeTemperature() != null)  result += "AIR INTAKE TEMP: " + this.getAirIntakeTemperature() + "C\n";
        if(this.getThrottlePosition() != null)  result += "THROTTLE: " + this.getThrottlePosition() + "%\n";
        if(this.getOilTemperature() != null)  result += "OIL TEMP: " + this.getOilTemperature() + "C\n";
        if(this.getEngineCoolantTemperature() != null)  result += "EC TEMP: " + this.getEngineCoolantTemperature() + "C\n";
        if(this.getFuelLevel() != null)  result += "FUEL LEVEL: " + this.getFuelLevel() + "%\n";
        if(this.getFuelConsumption() != null)  result += "FUEL CONSUMPTION: " + this.getFuelConsumption() + "l/h\n";

        Date date = new Date(getTime() * 1000L);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = sdf.format(date);
        result += "TIME: " + formattedDate + "\n";

        return result;
    }
}
