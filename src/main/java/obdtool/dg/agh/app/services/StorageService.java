package obdtool.dg.agh.app.services;

import java.text.SimpleDateFormat;
import java.util.Date;

import obdtool.dg.agh.app.utils.DataManager;
import obdtool.dg.agh.app.models.ObdData;
import obdtool.dg.agh.app.utils.enums.ObdParamType;

/**
 * Created by Damian on 06.11.2016.
 */

public class StorageService {

    private int saveInterval; //\ czas pomiędzy zapisem zebranych danych w bazie lokalnej (w sekundach)
    private StoringThread thread;
    private String tripName;

    public StorageService(){
        saveInterval = 3;
        tripName = null;
    }

    public boolean isEnabled() {
        return tripName != null;
    }

    public boolean startStoring(){
        if(!isEnabled()){
            thread = new StoringThread();
            thread.start();

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String timeStr = sdf.format(new Date());
            tripName = "TRIP " + timeStr;

            DataManager.log("started storing...");
            return true;
        } else {
            return false;
        }
    }

    public boolean stopStoring(){
        if(isEnabled()){
            tripName = null;

            if(thread != null){
                thread.interrupt();
                thread = null;
            }
            DataManager.log("storing stopped");
            return true;
        } else {
            return false;
        }
    }

    class StoringThread extends Thread {
        ObdData dataDao;

        public StoringThread(){}

        public void run(){
            try {
                while(true){

                    try{
                        dataDao = new ObdData(
                                DataManager.getLatitude(),
                                DataManager.getLongitude(),
                                tripName,
                                DataManager.getParam(ObdParamType.RPM).getValueAsInteger(),
                                DataManager.getParam(ObdParamType.SPEED).getValueAsDouble(),
                                DataManager.getParam(ObdParamType.ACU_VOLTAGE).getValueAsDouble(),
                                DataManager.getParam(ObdParamType.INTAKE_TEMP).getValueAsDouble(),
                                DataManager.getParam(ObdParamType.ENGINE_COOLANT_TEMP).getValueAsDouble(),
                                DataManager.getParam(ObdParamType.OIL_TEMP).getValueAsDouble(),
                                DataManager.getParam(ObdParamType.THROTTLE_POSITION).getValueAsDouble(),
                                DataManager.getParam(ObdParamType.FUEL_LEVEL).getValueAsDouble(),
                                DataManager.getParam(ObdParamType.FUEL_CONSUMPTION).getValueAsDouble(),
                                DataManager.getProfileId()
                        );

                        dataDao.save();
                        DataManager.log("inserted " + dataDao.getLatitude() + " " + DataManager.getLongitude());
                    }catch(Exception e){
                        DataManager.log(e.getMessage());
                    }

                    sleep(1000 * saveInterval);
                }
            } catch (InterruptedException e) {
                DataManager.log("storing thread stopped");
                e.printStackTrace();
            }
        }
    }
}
