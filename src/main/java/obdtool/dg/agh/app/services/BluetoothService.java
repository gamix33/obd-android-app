package obdtool.dg.agh.app.services;

import java.io.IOException;
import java.util.UUID;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.os.Message;

import obdtool.dg.agh.app.utils.DataManager;
import obdtool.dg.agh.app.utils.enums.ConnectionState;

public class BluetoothService {

    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private final BluetoothAdapter adapter;
    private final Handler handler;
    private ConnectThread connectThread;
    private static BluetoothSocket socket = null;

    public BluetoothService(Handler handler) {
        this.adapter = BluetoothAdapter.getDefaultAdapter();
        this.handler = handler;
    }

    public boolean isConnected(){
        return socket != null;
    }

    public void disconnect(){
        try {
            socket.close();
        } catch (IOException e) {
            DataManager.log("error socket close");
        }
    }

    public BluetoothSocket getSocket(){
        return socket;
    }

    private synchronized void setState(ConnectionState state, String text) {
        DataManager.setConnectionState(state);
        DataManager.log(text);

        Message msg = new Message();
        msg.what = DataManager.MSG_STATE_CHANGED;
        handler.sendMessage(msg);
    }

    public synchronized void connect(BluetoothDevice device) {
        if(socket == null){
            if (connectThread == null) {
                connectThread = new ConnectThread(device);
                connectThread.start();

                setState(ConnectionState.CONNECTING_ELM, "trying connect to " + device);
            }
        } else {
            connected(socket);
        }
    }

    public synchronized void connected(BluetoothSocket socket) {
        setState(ConnectionState.ELM_CONNECTED, "connected to device");

        Message msg = new Message();
        msg.obj = socket;
        msg.what = DataManager.MSG_BLUETOOTH_CONNECTED;
        handler.sendMessage(msg);
    }

    class ConnectThread extends Thread {

        public ConnectThread(BluetoothDevice device) {
            BluetoothSocket tmp = null;

            try {
                tmp = device.createInsecureRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) {
                setState(ConnectionState.FAILED, "creating socket failed");
            }

            socket = tmp;
        }

        public void run() {
            adapter.cancelDiscovery();

            try {
                socket.connect();
            } catch (IOException e) {
                setState(ConnectionState.FAILED, "connection failed");
                socket = null;
                return;
            }

            connected(socket);
        }
    }
}