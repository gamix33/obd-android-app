package obdtool.dg.agh.app.services;

import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.os.Message;

import com.github.pires.obd.commands.control.TroubleCodesCommand;
import com.github.pires.obd.commands.protocol.EchoOffCommand;
import com.github.pires.obd.commands.protocol.ObdResetCommand;
import com.github.pires.obd.commands.protocol.ResetTroubleCodesCommand;
import com.github.pires.obd.commands.protocol.SelectProtocolCommand;
import com.github.pires.obd.commands.protocol.ObdRawCommand;
import com.github.pires.obd.commands.ObdCommand;
import com.github.pires.obd.commands.SpeedCommand;
import com.github.pires.obd.commands.engine.RPMCommand;
import com.github.pires.obd.commands.engine.OilTempCommand;
import com.github.pires.obd.commands.engine.ThrottlePositionCommand;
import com.github.pires.obd.commands.temperature.AirIntakeTemperatureCommand;
import com.github.pires.obd.commands.temperature.AmbientAirTemperatureCommand;
import com.github.pires.obd.commands.temperature.EngineCoolantTemperatureCommand;
import com.github.pires.obd.commands.fuel.FuelLevelCommand;
import com.github.pires.obd.commands.fuel.ConsumptionRateCommand;
import com.github.pires.obd.enums.ObdProtocols;
import com.github.pires.obd.exceptions.NonNumericResponseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import obdtool.dg.agh.app.utils.DataManager;
import obdtool.dg.agh.app.utils.enums.ConnectionState;
import obdtool.dg.agh.app.utils.ObdParam;
import obdtool.dg.agh.app.utils.enums.ObdParamType;

public class ObdReaderService {
    private final Handler handler;

    private static ObdThread obdThread;
    private boolean configEnabled = true;

    public ObdReaderService(Handler handler){
        this.handler = handler;
    }

    private void log(String msg){
        DataManager.log(msg);
    }

    public void startRead(BluetoothSocket socket) {
        if(obdThread != null) {obdThread.interrupt();}

        obdThread = new ObdThread(socket);
        obdThread.start();

        setState(ConnectionState.CONNECTING_OBD, "reader started");
    }

    public void readTroubleCodes(BluetoothSocket socket){
        if(obdThread != null) {obdThread.interrupt();}

        obdThread = new ObdThread(socket);
        obdThread.readTroubleCodes();
        setState(ConnectionState.CONNECTING_OBD, "trouble codes reader started");
    }

    public void cleanTroubleCodes(){
        obdThread.cleanTroubleCodes();
    }

    private void setState(ConnectionState state, String text) {
        DataManager.setConnectionState(state);
        DataManager.log(text);

        Message msg = new Message();
        msg.what = DataManager.MSG_STATE_CHANGED;
        handler.sendMessage(msg);
    }

    private void updateParamOnView(ObdParam param){
        Message msg = new Message();
        msg.what = DataManager.MSG_UPDATE_PARAM;
        msg.obj = param;
        handler.sendMessage(msg);

        DataManager.updateParam(param.getType(), param);
    }

    private void updateTroubleCodes(String codes){
        Message msg = new Message();
        msg.what = DataManager.TROUBLE_CODES_UPDATE;
        handler.sendMessage(msg);

        DataManager.setTroubleCodes(codes);
    }

    private void deletedTroubleCodes(){
        Message msg = new Message();
        msg.what = DataManager.TROUBLE_CODES_DELETED;
        handler.sendMessage(msg);

        DataManager.setTroubleCodes(null);
        updateTroubleCodes(null);
    }

    public void stop(){
        if(obdThread != null){
            obdThread.interrupt();
            obdThread = null;
        }
        setState(ConnectionState.NONE, "obd service stopped");
    }

    class ObdThread extends Thread{
        private BluetoothSocket tSocket;

        private HashSet<ObdParam> params;
        private ArrayList<ObdCommand> configCommands;

        public ObdThread(BluetoothSocket tSocket) {
            this.tSocket = tSocket;

            params = new HashSet<>();

            ObdParam rpmParam = new ObdParam();
            rpmParam.setUnit("RPM");
            rpmParam.setType(ObdParamType.RPM);
            rpmParam.setCommand(new RPMCommand());
            params.add(rpmParam);

            ObdParam intakeTempParam = new ObdParam();
            intakeTempParam.setUnit("C");
            intakeTempParam.setType(ObdParamType.INTAKE_TEMP);
            intakeTempParam.setCommand(new AirIntakeTemperatureCommand());
            params.add(intakeTempParam);

            ObdParam oilTempParam = new ObdParam();
            oilTempParam.setUnit("C");
            oilTempParam.setType(ObdParamType.OIL_TEMP);
            oilTempParam.setCommand(new OilTempCommand());
            params.add(oilTempParam);

            ObdParam ecTempParam = new ObdParam();
            ecTempParam.setUnit("C");
            ecTempParam.setType(ObdParamType.ENGINE_COOLANT_TEMP);
            ecTempParam.setCommand(new EngineCoolantTemperatureCommand());
            params.add(ecTempParam);

            ObdParam throttleParam = new ObdParam();
            throttleParam.setUnit("%");
            throttleParam.setType(ObdParamType.THROTTLE_POSITION);
            throttleParam.setCommand(new ThrottlePositionCommand());
            params.add(throttleParam);

            ObdParam fuelLevelParam = new ObdParam();
            fuelLevelParam.setUnit("%");
            fuelLevelParam.setType(ObdParamType.FUEL_LEVEL);
            fuelLevelParam.setCommand(new FuelLevelCommand());
            params.add(fuelLevelParam);

            ObdParam consumptionRateCommand = new ObdParam();
            consumptionRateCommand.setUnit("L/h");
            consumptionRateCommand.setType(ObdParamType.FUEL_CONSUMPTION);
            consumptionRateCommand.setCommand(new ConsumptionRateCommand());
            params.add(consumptionRateCommand);

            ObdParam speedCommand = new ObdParam();
            speedCommand.setUnit("km/h");
            speedCommand.setType(ObdParamType.SPEED);
            speedCommand.setCommand(new SpeedCommand());
            params.add(speedCommand);

            ObdParam acuVoltageCommand = new ObdParam();
            acuVoltageCommand.setUnit("V");
            acuVoltageCommand.setType(ObdParamType.ACU_VOLTAGE);
            acuVoltageCommand.setCommand(new ObdRawCommand("AT RV"){
                public String getCalculatedResult(){
                    return super.getCalculatedResult().replace("V","");
                }
            });
            params.add(acuVoltageCommand);

            ObdParam troubleCodesCommand = new ObdParam();
            troubleCodesCommand.setType(ObdParamType.TROUBLE_CODES);
            troubleCodesCommand.setCommand(new TroubleCodesCommand());
            params.add(troubleCodesCommand);
        }

        public void config(){
            if(configEnabled){
                try{
                    configCommands = new ArrayList<>();

                    configCommands.add(new ObdRawCommand("ATD"));
                    configCommands.add(new ObdResetCommand());
                    configCommands.add(new EchoOffCommand());
                    configCommands.add(new ObdRawCommand("AT L0"));
                    configCommands.add(new ObdRawCommand("AT S0"));
                    configCommands.add(new ObdRawCommand("AT H0"));
                    configCommands.add(new SelectProtocolCommand(ObdProtocols.AUTO));
                    configCommands.add(new AmbientAirTemperatureCommand());

                    for(ObdCommand c:configCommands){
                        c.run(tSocket.getInputStream(), tSocket.getOutputStream());
                        DataManager.log("config " + c.getFormattedResult());
                        Thread.sleep(50);
                    }
                }catch(Exception e){
                    log("adapters config error \n" + e.getMessage());
                    e.printStackTrace();
                }
            }
        }

        public void run(){
            DataManager.log("initialize adapters");

            //config
            this.config();
            setState(ConnectionState.OBD_CONNECTED, "obd connected");

            while (!Thread.interrupted()) {
                try{
                    Thread.sleep(100);
                }catch(InterruptedException e){
                    DataManager.log("obd reader thread interrupted");
                }

                for(ObdParam param: params) {
                    readParam(param);
                }
            }
        }

        private void readParam(ObdParam obdParam){
            ObdCommand command = obdParam.getCommand();
            try {
                command.run(tSocket.getInputStream(), tSocket.getOutputStream());
                obdParam.setValue(command.getCalculatedResult());
                updateParamOnView(obdParam);
            } catch (NonNumericResponseException e) {
                log("adapters no numeric response error");
                e.printStackTrace();
            } catch (IOException e) {
                log("adapters io error" + e.getMessage());
                e.printStackTrace();
            } catch (InterruptedException e) {
                log("adapters interrupted error" + e.getMessage());
                e.printStackTrace();
            } catch (Exception e) {
                log("adapters unknown error" + e.getMessage());
                e.printStackTrace();
            }
        }

        private void readTroubleCodes(){
            this.config();

            ObdParam troubleCodesCommand = new ObdParam();
            troubleCodesCommand.setType(ObdParamType.TROUBLE_CODES);
            troubleCodesCommand.setCommand(new TroubleCodesCommand());
            String result = null;
            try {
                ObdCommand command = troubleCodesCommand.getCommand();
                command.run(tSocket.getInputStream(), tSocket.getOutputStream());
                result = command.getFormattedResult();
            } catch (NonNumericResponseException e) {
                log("adapters no numeric response error \n");
                e.printStackTrace();
            } catch (IOException e) {
                log("adapters io error" + e.getMessage());
                e.printStackTrace();
            } catch (InterruptedException e) {
                log("adapters interrupted error" + e.getMessage());
                e.printStackTrace();
            } catch (Exception e) {
                log("adapters unknown error" + e.getMessage());
                e.printStackTrace();
            }

            updateTroubleCodes(result);
        }

        private void cleanTroubleCodes(){
            ObdCommand command = new ResetTroubleCodesCommand();
            try {
                command.run(tSocket.getInputStream(), tSocket.getOutputStream());
                String result = command.getFormattedResult();
                log("clean command result: " + result);
                deletedTroubleCodes();
            } catch (NonNumericResponseException e) {
                log("adapters no numeric response error \n");
                e.printStackTrace();
            } catch (IOException e) {
                log("adapters io error" + e.getMessage());
                e.printStackTrace();
            } catch (InterruptedException e) {
                log("adapters interrupted error" + e.getMessage());
                e.printStackTrace();
            } catch (Exception e) {
                log("adapters unknown error" + e.getMessage());
                e.printStackTrace();
            }
        }
    }
}
