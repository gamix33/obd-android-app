package obdtool.dg.agh.app.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.widget.Toast;

import obdtool.dg.agh.app.utils.DataManager;

public class GpsService extends Service implements LocationListener {

    Location location;
    double latitude;
    double longitude;

    boolean isGPSEnabled;
    boolean isNetworkEnabled;

    private LocationManager locationManager;
    private Context context;
    private Handler handler;

    private static final long UPDATE_DISTANCE = 5; // 5 metrów
    private static final long UPDATE_TIME = 1000 * 30; // 30 sekund

    public GpsService(Context context, Handler handler){
        this.context = context;
        this.handler = handler;
    }

    public boolean startGps() throws SecurityException{
        locationManager = (LocationManager) this.context.getSystemService(Context.LOCATION_SERVICE);

        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if(!isNetworkEnabled && !isGPSEnabled){
            Toast toast = Toast.makeText(context, "Włącz moduł GPS aby rejestrować trasę", Toast.LENGTH_SHORT);
            toast.show();
            DataManager.setTripTrackingEnabled(false);

            return false;
        }

        if(isNetworkEnabled){
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, UPDATE_TIME, UPDATE_DISTANCE, this);
            if (locationManager != null) {
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    DataManager.setTripTrackingEnabled(true);
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                }
            }
        }

        if(isGPSEnabled){
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, UPDATE_TIME, UPDATE_DISTANCE, this);
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                DataManager.setTripTrackingEnabled(true);
                latitude = location.getLatitude();
                longitude = location.getLongitude();
            }
        }

        return true;
    }

    public boolean stopGps(){
        locationManager = null;
        DataManager.setTripTrackingEnabled(false);

        return true;
    }

    @Override
    public void onLocationChanged(Location location) {
        Message msg = new Message();
        msg.what = DataManager.MSG_LOCATION_CHANGED;
        msg.obj = location;
        handler.sendMessage(msg);

        DataManager.setLatitude(location.getLatitude());
        DataManager.setLongitude(location.getLongitude());
    }


    @Override
    public void onProviderDisabled(String provider) {
    }


    @Override
    public void onProviderEnabled(String provider) {
    }


    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }


    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
}