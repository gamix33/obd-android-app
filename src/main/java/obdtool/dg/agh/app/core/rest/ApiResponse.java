package obdtool.dg.agh.app.core.rest;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.IOException;

import okhttp3.Response;


/**
 * Created by Damian on 04.12.2016.
 */

public class ApiResponse {
    private int code;
    private String errorMessage;
    private JsonObject bodyObject;
    private JsonArray bodyArray;
    private String bodyString;

    public ApiResponse(Response response) throws IOException {
        Gson gson = new Gson();

        code = response.code();
        bodyString = response.body().string();
        try {
            bodyObject = gson.fromJson(bodyString, JsonObject.class);
        } catch (Exception e){
            //parse exception if response is array
            Log.d("API RESPONSE", "response parse error");
        }
        if(code != 200){
            if(bodyObject != null){
                errorMessage = bodyObject.get("message").getAsString();
            }
        } else if(bodyObject == null){
            try{
                bodyArray = gson.fromJson(bodyString, JsonArray.class);
            } catch (Exception e){
                //parse exception if response is object
                Log.d("API RESPONSE", "response parse error");
            }
        }
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        code = code;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public JsonObject getBodyObject() {
        return bodyObject;
    }

    public void setBodyObject(JsonObject bodyObject) {
        bodyObject = bodyObject;
    }

    public void setErrorMessage(String errorMessage) {
        errorMessage = errorMessage;
    }

    public JsonArray getBodyArray() {
        return bodyArray;
    }

    public void setBodyArray(JsonArray bodyArray) {
        bodyArray = bodyArray;
    }

    public String getBodyString() {
        return bodyString;
    }

    public void setBodyString(String bodyString) {
        this.bodyString = bodyString;
    }
}