package obdtool.dg.agh.app.core;

import android.app.Application;
import android.content.Context;

import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.orm.SugarContext;
import com.orm.SugarRecord;

import okhttp3.OkHttpClient;

public class App extends Application {
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        SugarContext.init(mContext);

        Stetho.initializeWithDefaults(this);
        new OkHttpClient.Builder()
                .addNetworkInterceptor(new StethoInterceptor())
                .build();
    }

    @Override
    public void onTerminate(){
        SugarContext.terminate();
    }

    public static Context getContext(){
        return mContext;
    }
}
