package obdtool.dg.agh.app.core.ui;

public class DrawerItem{

    public int icon;
    public String name;

    public DrawerItem(int icon, String name){
        this.icon = icon;
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}