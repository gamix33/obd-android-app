package obdtool.dg.agh.app.core.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.widget.EditText;

import obdtool.dg.agh.app.R;

public abstract class PromptDialog extends AlertDialog.Builder implements OnClickListener {
    private final EditText inputTextView;

    public PromptDialog(Context context, int title, int message, String value) {
        super(context);
        setTitle(title);
        setMessage(message);

        inputTextView = new EditText(context);
        inputTextView.setText(value);
        setView(inputTextView);

        setPositiveButton(R.string.ok, this);
        setNegativeButton(R.string.cancel, this);
    }

    public void onCancelClicked(DialogInterface dialog) {
        dialog.dismiss();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == DialogInterface.BUTTON_POSITIVE) {
            if (onOkClicked(inputTextView.getText().toString())) {
                dialog.dismiss();
            }
        } else {
            onCancelClicked(dialog);
        }
    }

    abstract public boolean onOkClicked(String input);
}