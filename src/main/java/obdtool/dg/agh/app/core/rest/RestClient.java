package obdtool.dg.agh.app.core.rest;


import com.google.gson.Gson;

import java.io.IOException;

import obdtool.dg.agh.app.utils.DataManager;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RestClient {
    private static final String API_BASE_URL = "http://155.133.47.82:9001/";
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private static final Gson gson = new Gson();

    private static OkHttpClient client = new OkHttpClient();

    private static String getFullUrl(String url) {
        return API_BASE_URL + url;
    }

    public static ApiResponse get(String url) throws IOException {
        String authToken = DataManager.getAuthToken() != null ? DataManager.getAuthToken().toString() : "";
        Request request = new Request.Builder()
                .url(getFullUrl(url))
                .header("Authorization", authToken)
                .header("Content-Type", "application/json")
                .build();

        Response response = client.newCall(request).execute();
        return new ApiResponse(response);
    }

    public static ApiResponse post(String url, Object data) throws IOException {
        RequestBody body = RequestBody.create(JSON, gson.toJson(data));
        String authToken = DataManager.getAuthToken() != null ? DataManager.getAuthToken().toString() : "";
        Request request = new Request.Builder()
                .url(getFullUrl(url))
                .header("Authorization", authToken)
                .post(body)
                .build();

        Response response = client.newCall(request).execute();
        return new ApiResponse(response);
    }

    public static ApiResponse delete(String url) throws IOException {
        String authToken = DataManager.getAuthToken() != null ? DataManager.getAuthToken().toString() : "";
        Request request = new Request.Builder()
                .url(getFullUrl(url))
                .header("Authorization", authToken)
                .header("Content-Type", "application/json")
                .delete()
                .build();

        Response response = client.newCall(request).execute();
        return new ApiResponse(response);
    }

}

